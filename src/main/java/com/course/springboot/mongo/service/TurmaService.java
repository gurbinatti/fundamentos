package com.course.springboot.mongo.service;

import com.course.springboot.mongo.entity.Turmas;
import com.course.springboot.mongo.repository.TurmasRepository;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.lang.String.format;

@Service
public class TurmaService {

    private final TurmasRepository turmasRepository;
    private static final String ERRO = "ERRO: ";

    TurmaService(
            final TurmasRepository turmasRepository
    ) {
        this.turmasRepository = turmasRepository;
    }

    public void recuperaTodasTurmas() {
        List<Turmas> turmas = this.turmasRepository.findAll();

        for (Turmas turma: turmas) {
            System.out.println(
                    format("ID: %s\n" +
                            "%s SERIE %s\n" +
                            "-------------------------------------------------------------------- " +
                            "POSICAO: %s" +
                            " --------------------------------------------------------------------",
                            turma.getId(), turma.getSerie(), turma.getLetra(),
                            turmas.indexOf(turma)
                    )
            );
        }

        System.out.println("\n >>>> TOTAL Turmas: " + turmas.size());
    }

    public void cadastraUnicaTurma(Turmas turma){
        try {
            turma.setId(new ObjectId());
            System.out.println(
                    "HEXSTRING: " +
                    this.turmasRepository.save(turma).getId().toHexString() //-- RECUPERA O VALOR EM STRING DO ObjectId
            );
        } catch (RuntimeException erro) {
            System.out.println(ERRO + erro.getMessage());
        }
    }

    public void deletaTurmas(String hexString) {
        try {
            this.turmasRepository.deleteById(new ObjectId(hexString));
        } catch (RuntimeException erro) {
            System.out.println(ERRO + erro.getMessage());
        }
    }
}
