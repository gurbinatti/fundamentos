package com.course.springboot.mongo.service;

import com.course.springboot.mongo.entity.Alunos;
import com.course.springboot.mongo.entity.dtos.AlunoEmail;
import com.course.springboot.mongo.repository.AlunosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
public class AlunosService {

    @Autowired
    private final AlunosRepository alunosRepository;

    private static final String LINHAS = "--------------------------------------------------------------------";
    private static final String REGISTROS = ">>>TOTAL DE REGISTROS: ";
    private static final String ERRO = "ERRO: ";
    private static final String NOVA_LINHA = "\n";

    AlunosService(
            final AlunosRepository alunosRepository
    ) {
        this.alunosRepository = alunosRepository;
    }

    public void recuperaTodosAlunos() {
        List<Alunos> alunos = this.alunosRepository.findAll();
        String msg = "";
        for (Alunos aluno: alunos) {
            msg = format("ID: %s%s" +
                    "NOME: %s%s" +
                    "EMAIL: %s%s" +
                    "NASCIMENTO: %s%s" +
                    "NOTAS: %s%s" +
                    "CURSOS: %s%s" +
                    "HABILIDADES: %s%s" +
                    "LOCALIZACAO: %s%s",
                    aluno.getId(), NOVA_LINHA, aluno.getNome(), NOVA_LINHA,
                    aluno.getEmail(), NOVA_LINHA, aluno.getDataNascimento(), NOVA_LINHA,
                    aluno.getNotas(), NOVA_LINHA, aluno.getCurso(), NOVA_LINHA,
                    aluno.getHabilidades(), NOVA_LINHA, aluno.getLocalizacao(), NOVA_LINHA);
            logList(msg, alunos.indexOf(aluno));
        }

        System.out.println(NOVA_LINHA + REGISTROS + alunos.size());
    }

    public void cadastraUnicoAluno(Alunos aluno){
        try {
            aluno.setId(UUID.randomUUID());
            this.alunosRepository.save(aluno);
        } catch (RuntimeException erro) {
            System.out.println(ERRO + erro.getMessage());
        }
    }

    public void deletaAluno(String id) {
        try {
            this.alunosRepository.deleteById(UUID.fromString(id));
        } catch (RuntimeException erro) {
            System.out.println(ERRO + erro.getMessage());
        }
    }

    public void recuperaAlunoPorEmailOrderNome(String email){
        List<AlunoEmail> alunosEmail = this.alunosRepository.findUserShortByEmail(email);
        String msg = "";

        for (AlunoEmail alunoEmail: alunosEmail) {
            msg = format(" NOME: %s %s EMAIL: %s%s",
                    alunoEmail.getNome(), NOVA_LINHA,
                    alunoEmail.getEmail(), NOVA_LINHA);

            logList(msg, alunosEmail.indexOf(alunoEmail));
        }

        System.out.println(NOVA_LINHA + REGISTROS + alunosEmail.size() +
                NOVA_LINHA + ">>>FILTRO EMAIL: " + email);
    }


    private void logList(String msg, int indeOf) {
        String msgInicial = "";
        if(indeOf == 0) {
            msgInicial = NOVA_LINHA + NOVA_LINHA;
            msgInicial += LINHAS;
            msgInicial += "INICIO LOG";
            msgInicial += LINHAS + NOVA_LINHA;
        }

        System.out.println(
                format(
                    "%s%s%sPOSICAO: %s%s",
                    msgInicial,
                    msg,
                        LINHAS, indeOf, LINHAS
                )
        );
    }
}
