package com.course.springboot.mongo.entity.dtos;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@Builder
@TypeAlias("dtoalunoemail")
public class AlunoEmail {
    private String nome;
    private String email;
}
