package com.course.springboot.mongo.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TypeAlias("alunos")
public class Alunos {
    @Id
    private UUID id;

    private String nome;

    @JsonProperty("data_nascimento")
    private LocalDateTime dataNascimento;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object curso;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Float> notas;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object habilidades;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object localizacao;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String email;
}
