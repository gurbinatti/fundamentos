package com.course.springboot.mongo.controller;

import com.course.springboot.mongo.entity.Turmas;
import com.course.springboot.mongo.service.TurmaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/turmas")
public class TurmaController {
    @Autowired
    private TurmaService turmaService;

    @GetMapping
    public void listaTurmas() {
        this.turmaService.recuperaTodasTurmas();
    }

    @PostMapping
    public void cadastraTurma(@RequestBody Turmas turma)
    {
        this.turmaService.cadastraUnicaTurma(turma);
    }

    @DeleteMapping
    public void deletaTurma(
        @RequestParam(value= "hexString", required = true) String hexString
    ) {
        this.turmaService.deletaTurmas(hexString);
    }
}
