package com.course.springboot.mongo.controller;

import com.course.springboot.mongo.entity.Alunos;
import com.course.springboot.mongo.service.AlunosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/alunos")
public class AlunoController {
    @Autowired
    private AlunosService alunosService;

    @GetMapping
    public void listaAlunos() {
        this.alunosService.recuperaTodosAlunos();
    }

    @GetMapping("/email")
    public void listaAlunosEmail(@RequestParam String email) {
        this.alunosService.recuperaAlunoPorEmailOrderNome(email);
    }

    @PostMapping
    public void cadastraAluno(@RequestBody Alunos aluno)
    {
        this.alunosService.cadastraUnicoAluno(aluno);
    }

    @DeleteMapping
    public void deletaAluno(@RequestParam String id) {
        this.alunosService.deletaAluno(id);
    }
}


