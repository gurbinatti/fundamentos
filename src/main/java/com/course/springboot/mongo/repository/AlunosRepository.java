package com.course.springboot.mongo.repository;

import com.course.springboot.mongo.entity.Alunos;
import com.course.springboot.mongo.entity.dtos.AlunoEmail;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.UUID;

public interface AlunosRepository extends MongoRepository<Alunos, UUID> {

    @Query(value="{ 'email' : ?0 }", fields="{ 'nome' : 1,  'email' : 1}")
    List<AlunoEmail> findUserShortByEmail(String email);
}
