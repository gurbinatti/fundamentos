package com.course.springboot.mongo.repository;

import com.course.springboot.mongo.entity.Turmas;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TurmasRepository extends MongoRepository<Turmas, ObjectId> {}
