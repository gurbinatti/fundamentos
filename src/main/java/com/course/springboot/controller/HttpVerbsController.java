package com.course.springboot.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("crud")
public class HttpVerbsController {
    @PostMapping("/value")
    public String createValue() {
        return "Cria " + " / POST";
    }

    @GetMapping("/value")
    public String getValue() {
        return "Recupera " + " / GET";
    }

    @PutMapping("/value")
    public String allValue(){
        return "Altera tudo" + " / PUT";
    }

    @PatchMapping("/value")
    public String partial(){
        return "Altera parcialmente" + " / PATCH";
    }

    @DeleteMapping("/value")
    public String deleteValue() {
        return "Exclui" + " / DELETE";
    }

}
