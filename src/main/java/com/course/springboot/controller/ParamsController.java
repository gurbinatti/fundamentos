package com.course.springboot.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/options")
public class ParamsController {

    @GetMapping("/{id}/path")
    public String path(
            @PathVariable Long id
    ) {
        return "PATH, meu ID --> " + id;
    }

    @GetMapping("/param")
    public String param(
        @RequestParam(value= "otherId", required = true) Long otherId,
        @RequestParam(value= "nomeSobrenome", required = true) String nome
    ) {
        return "PARAM, meu ID --> " + otherId + " NOME: " +  nome;
    }

    @GetMapping("/body")
    public String body(@RequestBody Map<String, Object> map) {
        return "Body, meu ID --> " + map.get("id") + " NOME: " + map.get("nome");
    }
}
