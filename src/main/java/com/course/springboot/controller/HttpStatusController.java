package com.course.springboot.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class HttpStatusController {
    @GetMapping("/re500")
    public ResponseEntity<String> get500() throws ResponseStatusException {
        System.out.println("teste");
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping("/re202")
    public ResponseEntity<String> get202() {
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping("/re404")
    public ResponseEntity<String> post404() throws ResponseStatusException {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/re400")
    public ResponseEntity<String> put400() throws ResponseStatusException {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
