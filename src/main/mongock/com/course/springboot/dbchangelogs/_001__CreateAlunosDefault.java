package com.course.springboot.dbchangelogs;

import com.course.springboot.mongo.entity.Alunos;

import collections.HabilidadesObject;
import collections.LocalizacaoObject;
import io.mongock.api.annotations.ChangeUnit;
import io.mongock.api.annotations.Execution;
import io.mongock.api.annotations.RollbackExecution;
import lombok.extern.slf4j.Slf4j;

import org.bson.Document;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Slf4j
@ChangeUnit(id="CreateAlunosDefault", order = "1", author = "Guilherme Urbinatti") //, runAlways=true)
public class _001__CreateAlunosDefault {
    private final MongoTemplate mongoTemplate;

    public _001__CreateAlunosDefault (
            MongoTemplate mongoTemplate
    ) {
            this.mongoTemplate = mongoTemplate;
    }

    @Execution
    public void aluno1() {
        log.info("Criando Aluno1");
        mongoTemplate.save(getAluno(), "alunos");
    }

    @RollbackExecution
    public void rollback() {
        mongoTemplate.remove(new Document());
    }

    private Alunos getAluno() {
        return Alunos.builder()
                .id(UUID.randomUUID())
                .nome("TESTE 3")
                .dataNascimento(LocalDateTime.now())
                .curso(createCursoObject("nome","MongoDB"))
                .notas(Arrays.asList(10.0f, 9.5f, 4.0f))
                .habilidades(createHabilidadeObject())
                .localizacao(createLocalizacaoObject())
                .email("exemplo@gmail.com")
                .build();
    }

    private Map<String,Object> createCursoObject(String key, String value) {
        Map<String,Object> curso = new HashMap<String, Object>();
        curso.put(key, value);
        return curso;
    }

    private List<HabilidadesObject> createHabilidadeObject() {
        return List.of(
            HabilidadesObject
                .builder()
                    .nome("Portugues")
                    .nivel("Avancado")
                .build(),
            HabilidadesObject
                .builder()
                    .nome("Ingles")
                    .nivel("Iniciante")
                .build()
        );

    }

    private LocalizacaoObject createLocalizacaoObject() {
        return LocalizacaoObject
                .builder()
                    .type("Point")
                    .endereco("Avenida K, 973")
                    .cidade("Orlandia")
                    .coodinates(List.of(-20.714434493127193,-47.87050331543591))
                .build();
    }
}




