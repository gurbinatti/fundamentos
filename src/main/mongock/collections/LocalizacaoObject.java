package collections;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
@Data
@Builder
@TypeAlias("localizacao")
public class LocalizacaoObject {
    private String endereco;
    private String cidade;
    private List<Double> coodinates;
    private String type;
}
