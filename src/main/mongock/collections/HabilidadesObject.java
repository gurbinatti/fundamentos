package collections;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@Builder
@TypeAlias("habilidade")
public class HabilidadesObject {
    private String nome;
    private String nivel;
}
