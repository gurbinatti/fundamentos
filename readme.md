# Fundamentos API #
### controller ###
  * Status de uma Requisicao
    * 200
    * 400
    * 500
  * Verbos
    * GET
    * PUT
    * POST
    * DELETE
    * PATCH
  * Opcoes da Requisicao
      * PATH
      * PARAM
      * BODY

### mongo (dataBase) ###
  * CRUD
    * Create
    * Read
    * Update
    * Delete
  * mongock
      * Script Automatico

### Necessario ###
* Java 11
* Docker
* Maven - https://maven.apache.org/download.cgi
* Postman - https://www.postman.com/
* IntelliJ (recomendado)
* MongoDB Compass (recomendado)

#### Colecoes do postman em: src/main/postman v2.1####
Obs: Controller Mongo, apenas void. 
Resultado aparece no console. 
